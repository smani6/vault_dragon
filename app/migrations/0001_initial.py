# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ObjectStore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('obj_key', models.CharField(unique=True, max_length=1000, verbose_name='object key')),
                ('obj_value', models.CharField(max_length=1000, verbose_name='object value')),
                ('created_datetime', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='ObjectStoreHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('obj_key', models.CharField(max_length=1000, verbose_name='history object key')),
                ('obj_value', models.CharField(max_length=1000, verbose_name='history object value')),
                ('created_datetime', models.DateTimeField()),
            ],
        ),
    ]
