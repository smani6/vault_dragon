import logging
import calendar
import time
import json
import pytz
from django.views.generic import View
from django.http import Http404, HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from datetime import datetime
from .models import ObjectStore, ObjectStoreHistory
from django.http import JsonResponse
from django.utils.timezone import now as utcnow
from calendar import timegm as epoch
from datetime import datetime
from django.shortcuts import render
log = logging.getLogger(__name__)


class KeyValStore(View):

    def get(self, request, slug=None):
        '''
            Method to return the value for the given key or for given key and timestamp
            if timestamp is present:
                convert it into datetime obj and check the db with the key
                if result found - format and return the response
                else return no details found for given key and timestamp response
            else:
                check whether any value exists for given key
                if found return formatted response
                else return no details found for given key

            :param : arg1 : request - Input get request
            :param : arg2 : slug - key
            :type: arg1 : http get request
            :type: arg2 : string
            :return: JsonResponse
        '''

        log.info("Entering KeyValStore get method ")
        timestamp = request.GET.get('timestamp')
        if timestamp:
            log.debug("UnixTimestamp value in request : %s", timestamp)
            datetime_obj = datetime.utcfromtimestamp(int(timestamp)).replace(tzinfo=pytz.utc)
            log.info("converting timestamp to datetime object")
            obj_dts = ObjectStore.objects.filter(created_datetime=datetime_obj, obj_key=slug)
            if obj_dts.exists():
                log.info("Object exists in current table")
                return JsonResponse(self.get_return_data(obj_dts))
            else:
                log.info("Object exists in history table")
                past_details = ObjectStoreHistory.objects.filter(
                    created_datetime=datetime_obj,
                    obj_key=slug)
                if past_details:
                    return JsonResponse(self.get_return_data(past_details))
                return JsonResponse({'Msg': "No details found for the given key and timestamp"})
        else:
            log.debug("No timestamp in request and the request is get by key")
            object_key = slug
            log.debug("Key value in request : %s", object_key)
            object_details = ObjectStore.objects.filter(obj_key=object_key)
            log.debug("Object details : %s", object_details)
            if object_details:
                return JsonResponse(self.get_return_data(object_details))

            return JsonResponse({'Msg': "No details found for the given key"})

    def post(self, request):
        try:
            '''
                Method to save the given key/value pair in db
                if no data exits in the request - return 400 status with msg
                if given key exits in the db , move the existing values to history
                    table and update the new value with datetime
                    and return the Jsonresponse
                else:
                    create a new entry for given key / value pair in db
                    and return the Jsonresponse

                :param: request - containing key/value
                :type : http post request
                :returns : key, value, created_datetime and msg
                :raises: HttpResponseBadRequest
            '''
            log.info("Entering KeyValueStore Post Method")
            data = json.loads(request.body)
            log.info("Input Request data : %s", data)

            if not data:
                return HttpResponseBadRequest(
                    "Input Request is empty")

            for key, value in data.iteritems():
                object_key = key
                object_value = value

            if not object_key or not object_value:
                return HttpResponseBadRequest(
                    "Please check your data - an object key should be associated with the value")

            object_details = ObjectStore.objects.filter(obj_key=object_key)
            if object_details.exists():
                log.info("Object key already exits")
                log.info("Moving existing values into history table")
                ObjectStoreHistory.objects.create(
                    obj_key=object_details[0].obj_key,
                    obj_value=object_details[0].obj_value,
                    created_datetime=self.to_date(self.get_epoch()))

                modified_datetime = self.to_date(self.get_epoch())
                log.info("Updating value of existing key")
                object_details.update(obj_value=object_value, created_datetime=modified_datetime)

                log.info("Object updated successfully - returning jsonresponse")
                return JsonResponse({'key': object_key,
                                     'value': object_value,
                                     'modified_datetime': str((modified_datetime)),
                                     'Msg': "Object Already exists - so updating it"})

            else:
                log.info("Object not exits - creating it")
                created_datetime = self.to_date(self.get_epoch())
                create_obj = ObjectStore.objects.create(
                    obj_key=object_key,
                    obj_value=object_value, created_datetime=created_datetime)

                log.info("Object created successfully - returning jsonresponse")
                return JsonResponse({'key': create_obj.obj_key,
                                     'value': create_obj.obj_value,
                                     'created_datetime': str(created_datetime),
                                     'Msg': "Object created successfully"})

        except Exception as e:
            log.exception(e)

    def get_epoch(self):
        return calendar.timegm(time.gmtime())

    def to_date(self, epoch):
        # return time.gmtime(epoch / 1000)
        return datetime.utcfromtimestamp(epoch).replace(tzinfo=pytz.utc)

    def get_return_data(self, obj):
        return {'key': obj[0].obj_key, "value": obj[
            0].obj_value, 'created_datetime': str(obj[0].created_datetime)}
