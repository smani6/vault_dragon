from django.conf.urls import url
from .views import KeyValStore
urlpatterns = [
    url(r'^object$', KeyValStore.as_view(), name='key_store_obj'),
    url(r'^object/(?P<slug>\w+)', KeyValStore.as_view(), name='key_store'),
]
