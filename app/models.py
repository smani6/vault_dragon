from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now as utcnow
from calendar import timegm as epoch


class ObjectStore(models.Model):

    """
        Table to store the given key/value pair with created datetime of it
    """
    obj_key = models.CharField(_("object key"), max_length=1000, unique=True)
    obj_value = models.CharField(_("object value"), max_length=1000)
    created_datetime = models.DateTimeField()

    def __unicode__(self):
        return "key  %s - value  %s - created time %s" % (self.obj_key,
                                                          self.obj_value,
                                                          self.created_datetime)


class ObjectStoreHistory(models.Model):

    """
        Table to capture the history records of key/value pair
    """

    obj_key = models.CharField(_("history object key"), max_length=1000)
    obj_value = models.CharField(_("history object value"), max_length=1000)
    created_datetime = models.DateTimeField()

    def __unicode__(self):
        return "key  %s - value  %s - created time %s" % (self.obj_key,
                                                          self.obj_value,
                                                          self.created_datetime)
