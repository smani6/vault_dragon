from django.test import TestCase, RequestFactory
from .views import KeyValStore
from django import http, shortcuts
import mock
import json
from mock import patch
from mock import Mock
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import JsonResponse
# Create your tests here.


class KeyValueStoreTestCases(TestCase):

    def setUp(self):
        self.factory = RequestFactory()

    def test_get_value_by_key_no_details(self):
        """
            Method to test get by value, no details exists for the given key
        """
        response = self.client.get('/object/key1')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {"Msg": "No details found for the given key"})

    def test_post_key_value_success(self):
        """
            Method to post a key value pair to the api and test the response
        """

        data = {
            "fb": "Mark"
        }

        request_create = self.factory.post(
            reverse('key_store_obj'),
            json.dumps(data),
            content_type="application/json")
        response = KeyValStore.as_view()(request_create)

        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content)
        self.assertEqual(response['key'], "fb")
        self.assertEqual(response['value'], "Mark")
        self.assertEqual(response['Msg'], "Object created successfully")

    def test_post_key_value_sucess_and_update(self):
        """
            Method to test get by value - post the value and update it and test
            whether the new value saved successfully
        """

        data = {
            "fb": "Mark"
        }

        request_create = self.factory.post(
            reverse('key_store_obj'),
            json.dumps(data),
            content_type="application/json")
        response = KeyValStore.as_view()(request_create)

        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content)
        self.assertEqual(response['key'], "fb")
        self.assertEqual(response['value'], "Mark")
        self.assertEqual(response['Msg'], "Object created successfully")

        update_data = {"fb": "Zuker"}
        request_create = self.factory.post(
            reverse('key_store_obj'),
            json.dumps(update_data),
            content_type="application/json")

        response = KeyValStore.as_view()(request_create)

        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content)
        self.assertEqual(response['key'], "fb")
        self.assertEqual(response['value'], "Zuker")
        self.assertEqual(response['Msg'], "Object Already exists - so updating it")

    def test_post_no_key_value_failure(self):
        """
            Method to test post - when no data is present in the request
        """

        data = {

        }

        request_create = self.factory.post(
            reverse('key_store_obj'),
            json.dumps(data),
            content_type="application/json")

        response = KeyValStore.as_view()(request_create)

        self.assertEqual(response.status_code, 400)

        self.assertEqual(
            response.content,
            "Input Request is empty")

    def test_get_by_key_success(self):
        """
            Method to test get by key success - and assert the response
        """

        data = {
            "fb": "Mark"
        }

        request_create = self.factory.post(
            reverse('key_store_obj'),
            json.dumps(data),
            content_type="application/json")
        response = KeyValStore.as_view()(request_create)

        response = self.client.get('/object/fb')

        self.assertEqual(response.status_code, 200)

        response = json.loads(response.content)
        self.assertEqual(response['key'], "fb")
        self.assertEqual(response['value'], "Mark")
