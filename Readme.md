Application: Version controlled key-value store with a RESTful HTTP API 

Application hosted at Openshift : http://vaultdragon-githubusersdata.rhcloud.com/object

Services are based on REST API and details can be posted or retrieved 

POST: Accept a key/value pair

    URL : http://vaultdragon-githubusersdata.rhcloud.com/object
    data : {"key" : "value"}
    content-type : "application/json"

    If key doesn't exists - new entry would be created in db - with the created_dattime as request time
    If key already exists - value of the key will be updated 
    
    Request: http://vaultdragon-githubusersdata.rhcloud.com/object
    content-type: "application/json"
    body =	{"company" :"vault_dragon"}
    Response:
    {
      "created_datetime": "2016-09-28 09:34:03+00:00",
      "Msg": "Object created successfully",
      "value": "vault_dragon",
      "key": "company"
    }
    
    Updating value for existing key
    
    Request: http://vaultdragon-githubusersdata.rhcloud.com/object
    content-type: "application/json"
    body =	{"company" :"vault_dragon_new"}
    Response:
    {
      "Msg": "Object Already exists - so updating it",
      "modified_datetime": "2016-09-28 09:50:59+00:00",
      "value": "vault_dragon_new",
      "key": "company"
    }
        
GET : Retrieve value for given key

    URL : http://vaultdragon-githubusersdata.rhcloud.com/object/key
    
    Request:
    URL : http://vaultdragon-githubusersdata.rhcloud.com/object/key
    Response:
    {
      "created_datetime": "2016-09-28 09:50:59+00:00",
      "value": "vault_dragon_new",
      "key": "company"
    }
    
GET : Retrieve value for given key and timestamp

    URL : http://vaultdragon-githubusersdata.rhcloud.com/object?timestamp=1440568980
    
    Request:
        http://vaultdragon-githubusersdata.rhcloud.com/object?timestamp=1440568980
    Response:
       {
          "created_datetime": "2016-09-28 09:50:59+00:00",
          "value": "vault_dragon_new",
          "key": "company"
        } 